#!/usr/bin/env python
# -*- coding: utf-8 -*-

##################################################################################
##################################################################################
## simple_raw_socket_sniffer.py Software Developer SACKUFPI                     ##
## Academic example to show how to implement a simple sniffer using raw_sockets ##
## Yoandrys Peña Meriño                                                         ##
##                                                                              ##
##################################################################################

import socket
import struct
import binascii
import platform
import time
import uuid

ETHER_BROADCAST = "\xff" * 6
ETH_P_ETHER = 0x0001
ETH_P_IP = '\x08\x00'  # 0x0800
ETH_P_ARP = '\x08\x06'  # 0x0806
ETH_P_EVERYTHING = 0x0003


class SimpleARPParser:
    def __init__(self, data):
        arp_header = data[0][14:42]
        arp_hdr = struct.unpack("!2s2s1s1s2s6s4s6s4s", arp_header)

        self.hardware_type = binascii.hexlify(arp_hdr[0])
        self.protocol_type = binascii.hexlify(arp_hdr[1])
        self.hardware_size = binascii.hexlify(arp_hdr[2])
        self.protocol_size = binascii.hexlify(arp_hdr[3])
        self.opcode = binascii.hexlify(arp_hdr[4])
        self.source_mac = binascii.hexlify(arp_hdr[5])
        self.source_ip = socket.inet_ntoa(arp_hdr[6])
        self.dest_mac = binascii.hexlify(arp_hdr[7])
        self.dest_ip = socket.inet_ntoa(arp_hdr[8])

    def print_header(self):
        print("$$$$$$$$$$$$$$$ ARP HEADER $$$$$$$$$$$$$$$$$$$$")
        print("Hardware type:   ", self.hardware_type)
        print("Protocol type:   ", self.protocol_type)
        print("Hardware size:   ", self.hardware_size)
        print("Protocol size:   ", self.protocol_size)
        print("Opcode:          ", self.opcode)
        print("Source MAC:      ", self.source_mac)
        print("Source IP:       ", self.source_ip)
        print("Dest MAC:        ", self.dest_mac)
        print("Dest IP:         ", self.dest_ip)
        print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n")


class SimpleIPParser:
    def __init__(self, data):
        ip_header = data[0][14:34]  # 20 bytes
        ip_hdr = struct.unpack('!BBHHHBBH4s4s', ip_header)
        self.ip_version = ip_hdr[0] >> 4  # IPv4 = 100 or IPv6 = 110
        # for normal header size minimum ip_ihl is 5 word (20 bytes) and maximun 15 word (60 bytes) (word=32bits)
        self.ip_ihl = ip_hdr[0] & 0xF  # word number of 32 bits current in datagram
        self.ip_hdr_length = self.ip_ihl * 4  # length in bytes
        self.ip_tos = ip_hdr[1]
        self.ip_total_length = ip_hdr[2]
        self.ip_identifier = ip_hdr[3]
        self.ip_flags = ip_hdr[4] >> 13  # only 3 bits most significant
        self.ip_fragment_position = ip_hdr[4] & 0x1FFF  # only 13 bits least significant
        self.ip_ttl = ip_hdr[5]
        self.ip_proto_id = ip_hdr[6]
        self.protocols = self.get_constants('IPPROTO_')
        self.protocol = self.protocols[self.ip_proto_id]
        self.ip_sum_header_control = ip_hdr[7]
        self.ip_source_ipaddress = socket.inet_ntoa(ip_hdr[8])
        self.ip_dest_ipaddress = socket.inet_ntoa(ip_hdr[9])
        self.slice_init = 14 + self.ip_hdr_length

    def get_constants(self, prefix):
        """Create a dictionary mapping socket module constants to their names."""
        return dict((getattr(socket, n), n)
                    for n in dir(socket)
                    if n.startswith(prefix)
                    )


class SimpleTCPParser:
    def __init__(self, data, slice_init, ip_hdr_length):
        tcpheader = data[0][slice_init: slice_init + 20]
        tcp_hdr = struct.unpack('!HHLLHHHH', tcpheader)
        self.tcp_source_port = tcp_hdr[0]
        self.tcp_dest_port = tcp_hdr[1]
        self.tcp_sequence_number = tcp_hdr[2]
        self.tcp_acknowledge_number = tcp_hdr[3]
        self.tcp_header_length = tcp_hdr[4] >> 12  # number of 32 bits words in tcp
        self.tcp_total_header_length = self.tcp_header_length * 4  # total header length in bytes
        # self.tcp_reserved = 000 #for use in the future
        self.tcp_flags = tcp_hdr[4] & 0x1FF
        self.tcp_window_length = tcp_hdr[5]
        self.tcp_verification_sum = tcp_hdr[6]
        self.tcp_urgent_pointer = tcp_hdr[7]
        self.sum_eth_ip_tcp_headers = 14 + ip_hdr_length + self.tcp_total_header_length
        self.size_payload_in_tcp_package = len(data[0]) - self.sum_eth_ip_tcp_headers  # data size in tcp package
        self.raw_payload = data[0][self.sum_eth_ip_tcp_headers:]  # read all data in tcp package


class SimpleUDPParser:
    def __init__(self, data, slice_init, ip_hdr_length):
        udpheader = data[0][slice_init: slice_init + 8]  # 20]
        udp_hdr = struct.unpack('!HHHH', udpheader)
        self.udp_source_port = udp_hdr[0]
        self.udp_dest_port = udp_hdr[1]
        self.udp_header_payload_length = udp_hdr[2]
        self.udp_verification_sum = udp_hdr[3]
        self.sum_eth_ip_udp_headers = 14 + ip_hdr_length + 8
        self.size_payload_in_udp_package = len(data[0]) - self.sum_eth_ip_udp_headers  # data size in a tcp package
        self.raw_payload = data[0][self.sum_eth_ip_udp_headers:]  # read all data in an udp package


class SimpleICMPParser:
    def __init__(self, data):
        icmpheader = data[0][
                     34:42]  # icmp package is 8 bytes of icmp header + IP header + first 64 bist of the datagram
        icmp_hdr = struct.unpack('!B7s', icmpheader)
        self.icmp_type = icmp_hdr[0]
        self.icmp_description = 'ECHO REPLY' if self.icmp_type == 0 else ''


class SimpleEthernetFrameParser:
    def __init__(self, data):
        ethhead = data[0][:14]
        eth = struct.unpack('!6s6s2s', ethhead)
        self.dest_mac = binascii.hexlify(eth[0])
        self.source_mac = binascii.hexlify(eth[1])
        self.frame_type = eth[2]  # binascii.hexlify(eth[2])


class SimpleSniffer:

    def __init__(self):
        sock_family = socket.PF_PACKET if platform.system() == 'Linux' else socket.AF_INET
        self.sniff_sock = socket.socket(sock_family, socket.SOCK_RAW,
                                        socket.ntohs(ETH_P_EVERYTHING))  # socket.htons(0x806)) only for arp packet

    def start(self):
        while True:
            try:
                self.parse()
            except struct.error:
                print('Invalid Header')

    def parse(self):
        print('PROTO    SOURCE_MAC    SOURCE_IPADDRESS:PORT  ---------------->  DEST_MAC    DEST_IPADDRESS:PORT    TTL')
        pkt = self.sniff_sock.recvfrom(2048)
        simple_eth_parser = SimpleEthernetFrameParser(pkt)
        # ARP PARSING
        if simple_eth_parser.frame_type == ETH_P_ARP:
            simple_arp_parser = SimpleARPParser(pkt)
            simple_arp_parser.print_header()
        # IP PARSING
        elif simple_eth_parser.frame_type == ETH_P_IP:
            simple_ip_parser = SimpleIPParser(pkt)
            # TCP PARSING
            if simple_ip_parser.ip_proto_id == socket.IPPROTO_TCP:
                simple_tcp_parser = SimpleTCPParser(pkt, simple_ip_parser.slice_init,
                                                    simple_ip_parser.ip_hdr_length)

                print('%s     %s    %s:%s  ---------------->  %s    %s:%s    %s' % \
                      (simple_ip_parser.protocol[8:], simple_eth_parser.source_mac,
                       simple_ip_parser.ip_source_ipaddress,
                       simple_tcp_parser.tcp_source_port, simple_eth_parser.dest_mac,
                       simple_ip_parser.ip_dest_ipaddress,
                       simple_tcp_parser.tcp_dest_port, simple_ip_parser.ip_ttl))
            # UDP PARSING
            elif simple_ip_parser.ip_proto_id == socket.IPPROTO_UDP:
                simple_udp_parser = SimpleUDPParser(pkt, simple_ip_parser.slice_init,
                                                    simple_ip_parser.ip_hdr_length)

                print('%s     %s    %s:%s  ---------------->  %s    %s:%s    %s' % \
                      (simple_ip_parser.protocol[8:], simple_eth_parser.source_mac,
                       simple_ip_parser.ip_source_ipaddress, simple_udp_parser.udp_source_port,
                       simple_eth_parser.dest_mac, simple_ip_parser.ip_dest_ipaddress,
                       simple_udp_parser.udp_dest_port, simple_ip_parser.ip_ttl))
            # ICMP PARSING
            elif simple_ip_parser.ip_proto_id == socket.IPPROTO_ICMP:
                simple_icp_parser = SimpleICMPParser(pkt)

                print('%s     %s    %s  ---------------->  %s    %s    %s    TYPE=%s (%s)' % \
                      (simple_ip_parser.protocol[8:], simple_eth_parser.source_mac,
                       simple_ip_parser.ip_source_ipaddress,
                       simple_eth_parser.dest_mac, simple_ip_parser.ip_dest_ipaddress, simple_ip_parser.ip_ttl,
                       simple_icp_parser.icmp_type, simple_icp_parser.icmp_description))
            else:
                print('---------- Receiving packet for protocol %s ---------' % simple_ip_parser.protocol[8:])


    # THE BELLOW CODE IS ABOUT HOW TO DEVELOP A SIMPLE ARPSPOOFING CLIENT.
    # def change_mac_address(self, new_mac_address):
    #     #        ifconfig eth0 down
    #     #        ifconfig eth0 hw ether 00:80:48:BA:d1:30
    #     #        ifconfig eth0 up
    #     pass
    #
    # def get_local_mac_address(self, interface=None):
    #     #       ifconfig eth0 | grep HWaddr |cut -dH -f2|cut -d\  -f2
    #     #       00:26:6c:df:c3:95
    #     local_mac = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff) for i in range(0, 8 * 6, 8)][::-1])
    #     return local_mac
    #
    # def get_mac_address(self, ip_victims):
    #     mac_victims = dict.fromkeys(ip_victims)
    #     # return dict mac_victims = {'ip_a': 00:ab:fa:aa:01:20, 'ip_b':......}
    #     pass
    #
    # def arp_spoof(self, ip_poison, ip_victims=[], local_mac=None, interface="wlan0"):
    #     if ip_poison:
    #         sock_arp_spoof = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0806))
    #         sock_arp_spoof.bind((interface, socket.htons(0x0806)))
    #         if local_mac:
    #             mac_source = local_mac
    #         else:
    #             mac_source = self.get_local_mac_address()
    #
    #         code = '\x08\x06'
    #         htype = '\x00\x01'
    #         protype = '\x08\x00'
    #         hsize = '\x06'
    #         psize = '\x04'
    #         opcode = '\x00\x02'
    #         ip_poison_formated = socket.inet_aton(ip_poison)
    #         if ip_victims:
    #             mac_victims = self.get_mac_address(ip_victims)
    #             arp_victims = {}
    #             for ip_address in mac_victims:
    #                 ip_address_formated = socket.inet_aton(ip_address)
    #                 eth = mac_victims[ip_address] + mac_source + code
    #                 arp_victim = eth + htype + protype + hsize + psize + opcode + mac_source + ip_poison_formated + \
    #                              mac_victims[ip_address] + ip_address_formated
    #                 arp_victims[ip_address] = arp_victim
    #         else:
    #             pass  # send all ip network Ex:192.168.1.0  mac_dest = '\xFF\xFF\xFF\xFF\xFF\xFF'
    #
    #         while True:
    #             for arp_victim in arp_victims:
    #                 sock_arp_spoof.send(arp_victim)


if __name__ == '__main__':
    sniff_server = SimpleSniffer()
    sniff_server.start()
